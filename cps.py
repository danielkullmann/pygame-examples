""""
Measures CPS (clicks per second) in 5 second windows.
"""

import time

import pygame

pygame.init()
pygame.font.init()
screen = pygame.display.set_mode((0, 0), pygame.RESIZABLE)
width, height = pygame.display.get_window_size()
clock = pygame.time.Clock()
running = True
cps_counter = 0
cps = None
best_cps = None
last = time.time()

font = pygame.font.SysFont(pygame.font.get_default_font(), 50)

boxes = [
    [
        (width/2 - 100, height/2 - 200),
        (width/2 + 100, height/2 - 120)
    ],
    [
        (width/2 - 100, height/2 - 100),
        (width/2 + 100, height/2 - 20)
    ],
    [
        (width/2 - 100, height/2),
        (width/2 + 100, height/2 + 80)
    ],
    [
        (width/2 - 100, height/2 + 100),
        (width/2 + 100, height/2 + 180)
    ]

]


def blit_centered(screen: pygame.Surface, surface: pygame.Surface, box):
    box_width = box[1][0] - box[0][0]
    box_height = box[1][1] - box[0][1]

    x_padding = (box_width - surface.get_width()) / 2
    y_padding = (box_height - surface.get_height()) / 2

    screen.blit(surface, (box[0][0] + x_padding, box[0][1] + y_padding))


while running:
    screen.fill("white")

    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            # pygame.QUIT event means the user clicked X to close your window
            running = False
        elif event.type == pygame.MOUSEBUTTONDOWN:
            cps_counter += 1

    for ((tx, ly), (bx, ry)) in boxes:
        pygame.draw.line(
            screen,
            color="blue",
            width=4,
            start_pos=(tx, ly),
            end_pos=(tx, ry)
        )
        pygame.draw.line(
            screen,
            color="blue",
            width=4,
            start_pos=(tx, ly),
            end_pos=(bx, ly)
        )
        pygame.draw.line(
            screen,
            color="blue",
            width=4,
            start_pos=(bx, ly),
            end_pos=(bx, ry)
        )
        pygame.draw.line(
            screen,
            color="blue",
            width=4,
            start_pos=(tx, ry),
            end_pos=(bx, ry)
        )

    current = time.time()

    # Print timer
    surface = font.render(f"{current-last:.2f}", True, "blue")
    blit_centered(screen, surface, boxes[0])

    # Print current click counter
    surface = font.render(f"{cps_counter}", True, "blue")
    blit_centered(screen, surface, boxes[1])

    # Print latest CPS
    if current - last >= 5.00:
        cps = cps_counter / 5.0
        if best_cps is None or cps > best_cps:
            best_cps = cps
        cps_counter = 0
        last = current

    if cps is not None:
        surface = font.render(f"{cps:.2f}", True, "blue")
        blit_centered(screen, surface, boxes[2])

    if best_cps is not None:
        surface = font.render(f"{best_cps:.2f}", True, "red")
        blit_centered(screen, surface, boxes[3])

    pygame.display.flip()
    dt = clock.tick(60) / 1000

pygame.font.quit()
pygame.quit()
