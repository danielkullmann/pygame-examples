"""
Just display dots that bonuce around the screen
"""

import math
import random

import pygame


colors = list(pygame.colordict.THECOLORS.values())


class Settings:
    point_size = 5
    initial_points = 0
    creation_frequency = 1


class State:
    def __init__(self, screen_width, screen_height):
        self.running = True
        self.finished = False
        self.moving_points = []
        self.counter = 0


class Game:

    def distance(self, p1, p2):
        p1x, p1y = p1
        p2x, p2y = p2

        dx = abs(p1x-p2x)
        dy = abs(p1y-p2y)

        return math.sqrt(dx*dx + dy*dy)

    def random_color(self):
        return random.choice(colors)
    
    def new_point(self, state, screen_width, screen_height, force_enemy=False):
        xp = random.randint(100, screen_width-100)
        yp = random.randint(100, screen_height-100)
        xd = random.randint(40, 200)
        yd = random.randint(40, 200)

        state.moving_points.append((((xp, yp), (xd, yd)), self.random_color()))

    def reflect_line(self, moving_line, map_width, map_height, speed):
        """ Reflect a moving line across all borders.
        Parameters:
        - moving_line: tuple of (point, vector)
        - point is (int, int)
        - vector is (int, int)
        -.map_width: int
        - map_height: int

        Returns: tuple of:
        - a new moving_line, i.e. a tuple of (point, vector)
        """

        while True:
            ((xp, yp), (xd, yd)) = moving_line

            xe = xp + xd * speed
            ye = yp + yd * speed

            in_bounds_x = xe >= 0 and xe < map_width
            in_bounds_y = ye >= 0 and ye < map_height

            if in_bounds_x and in_bounds_y:
                # Moved point is still on screen, so just return the moved
                # point with the same direction
                # TODO If end point (xe, ye) is on border, the direction
                # should be reversed already
                return ((xe, ye), (xd, yd))

            bounding_box = pygame.Rect(0, 0, map_width, map_height)
            line = bounding_box.clipline(xp, yp, xe, ye)

            (xp, yp) = line[1]

            if xp == 0 and xd < 0:
                # reflect across x == 0 line
                xd = - xd
            elif xp == map_width - 1 and xd > 0:
                # reflect across x == map_width - 1 line
                xd = - xd
            if yp == 0 and yd < 0:
                # reflect across y == 0 line
                yd = - yd
            elif yp == map_height - 1 and yd > 0:
                # reflect across y == map_height - 1 line
                yd = - yd

            moving_line = ((xp, yp), (xd, yd))


def main():
    pygame.init()
    pygame.font.init()

    screen = pygame.display.set_mode((0, 0), pygame.RESIZABLE)
    clock = pygame.time.Clock()

    font = pygame.font.SysFont(pygame.font.get_default_font(), 50)

    dt = 1

    screen_width = screen.get_width()
    screen_height = screen.get_height()

    game = Game()
    state = State(screen_width, screen_height)
    for _ in range(Settings.initial_points):
        game.new_point(state, screen_width, screen_height)

    while state.running:

        screen.fill("white")

        if state.finished:
            new_game = False

            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    state.running = False
                if event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_q:
                        state.running = False
                    if event.key == pygame.K_n:
                        state = State(screen_width, screen_height)
                        for _ in range(Settings.initial_points):
                            game.new_point(state, screen_width, screen_height)

            if not new_game:
                continue

        state.counter += 1
        if Settings.creation_frequency > 0 and \
                state.counter % Settings.creation_frequency == 0:
            game.new_point(state, screen_width, screen_height)

        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                state.running = False

        new_moving_points = []

        for (moving_point, color) in state.moving_points:

            next = game.reflect_line(
                moving_point,
                screen_width,
                screen_height,
                dt
            )

            pygame.draw.circle(screen, color, next[0], Settings.point_size)

            new_moving_points.append((next, color))

        state.moving_points = new_moving_points

        surface1 = font.render(
            f"Punkte: {len(state.moving_points)}",
            True,
            "black"
        )

        surface2 = font.render(
            f"Delta: {dt:.2f}",
            True,
            "black"
        )

        screen.blit(surface1, (0, 0))
        screen.blit(surface2, (250, 0))

        # flip() the display to put your work on screen
        pygame.display.flip()

        # limits FPS to 60
        # dt is delta time in seconds since last frame, used for framerate-
        # independent physics.
        dt = clock.tick(60) / 1000

    pygame.quit()


if __name__ == '__main__':
    main()
