"""
Click on screen, move mouse, then release
This defines a vector for a point to move
(moving_points). On each iteration, those
moving points are moved, resulting in a line.
Hitting the wall reflects the line
"""

import random

import pygame

colors = ["blue", "red", "green", "black", "purple"]


def reflect_line(moving_line, map_width, map_height, speed):
    """ Reflect a moving line across all borders.
    Parameters:
    - moving_line: tuple of (point, vector)
      - point is (int, int)
      - vector is (int, int)
    -.map_width: int
    - map_height: int

    Returns: tuple of:
    - a new moving_line, i.e. a tuple of (point, vector)
    - drawn lines
    """
    bounding_box = pygame.Rect(0, 0, map_width, map_height)
    lines = []

    while True:
        ((xp, yp), (xd, yd)) = moving_line

        xe = xp + xd * speed
        ye = yp + yd * speed

        if bounding_box.contains(pygame.Rect(xe, ye, 1, 1)):
            """ Moved point is still on screen, so just return the moved
            point with the same direction"""
            # TODO If end point (xe, ye) is on border, the direction
            # should be reversed already
            lines.append(((xp, yp), (xe, ye)))
            return (((xe, ye), (xd, yd)), lines)

        line = bounding_box.clipline(xp, yp, xe, ye)

        lines.append(line)

        rest_line = (line[1], (xe, ye))

        (xp, yp), (xc, yc) = rest_line

        if xp == 0:
            # reflect across x == 0 line
            xd = - xd
        elif xp == map_width - 1:
            # reflect across x == map_width - 1 line
            xd = - xd
        if yp == 0:
            # reflect across y == 0 line
            yd = - yd
        elif yp == map_height - 1:
            # reflect across y == map_height - 1 line
            yd = - yd

        moving_line = ((xp, yp), (xd, yd))


def main():
    # pygame setup
    pygame.init()

    screen = pygame.display.set_mode((1280, 720))
    clock = pygame.time.Clock()
    dt = 1
    running = True

    moving_points = []

    line_start = None

    screen_width = screen.get_width()
    screen_height = screen.get_height()

    player = [screen_width/2, screen_height/2]
    player_move = [0, 0]
    speed = 2

    counter = 0

    while running:

        counter += 1
        if counter % 60 == 0:
            xp = random.randint(100, screen_width-100)
            yp = random.randint(100, screen_height-100)
            xd = random.randint(240, 480)
            yd = random.randint(240, 480)
            moving_points.append((((xp, yp), (xd, yd)), "blue"))

        screen.fill("white")

        new_moving_points = []

        for moving_point, color in moving_points:
            next, lines = reflect_line(
                moving_point,
                screen_width,
                screen_height,
                dt
            )
            new_moving_points.append((next, color))

            for a, b in lines:
                pygame.draw.line(screen, color, a, b, 5)

        moving_points = new_moving_points

        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                running = False
            elif event.type == pygame.MOUSEBUTTONDOWN:
                line_start = pygame.mouse.get_pos()
            elif event.type == pygame.MOUSEBUTTONUP:
                pos = pygame.mouse.get_pos()
                moving_points.append((
                    (line_start, (pos[0]-line_start[0], pos[1]-line_start[1])),
                    random.choice(colors)
                ))
                line_start = None
            elif event.type == pygame.KEYDOWN:
                if event.key == pygame.K_DOWN:
                    player_move[1] += 1
                elif event.key == pygame.K_UP:
                    player_move[1] -= 1
                elif event.key == pygame.K_LEFT:
                    player_move[0] -= 1
                elif event.key == pygame.K_RIGHT:
                    player_move[0] += 1
            elif event.type == pygame.KEYUP:
                if event.key == pygame.K_DOWN:
                    player_move[1] -= 1
                elif event.key == pygame.K_UP:
                    player_move[1] += 1
                elif event.key == pygame.K_LEFT:
                    player_move[0] += 1
                elif event.key == pygame.K_RIGHT:
                    player_move[0] -= 1

        player = [
            player[0] + player_move[0] * speed,
            player[1] + player_move[1] * speed
        ]
        if player[0] < 5:
            player[0] = 5
        elif player[0] >= screen_width-6:
            player[0] = screen_width-6
        if player[1] < 5:
            player[1] = 5
        elif player[1] >= screen_height-6:
            player[1] = screen_height-6

        pygame.draw.circle(screen, "red", player, 8)

        # flip() the display to put your work on screen
        pygame.display.flip()

        # limits FPS to 60
        # dt is delta time in seconds since last frame, used for framerate-
        # independent physics.
        dt = clock.tick(60) / 1000

    pygame.quit()


if __name__ == '__main__':
    main()
