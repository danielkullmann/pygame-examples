"""
Click on screen, move mouse, then release
This defines a vector for a point to move
(moving_points). On each iteration, those
moving points are moved, resulting in a line.
Hitting the wall reflects the line
"""
import pygame


def reflect_line(moving_line, map_width, map_height):
    """ Reflect a moving line across all borders.
    Parameters:
    - moving_line: tuple of (point, vector)
      - point is (int, int)
      - vector is (int, int)
    -.map_width: int
    - map_height: int

    Returns: tuple of:
    - a new moving_line, i.e. a tuple of (point, vector)
    - drawn lines
    """
    bounding_box = pygame.Rect(0, 0, map_width, map_height)
    lines = []

    while True:
        ((xp, yp), (xd, yd)) = moving_line

        xe = xp + xd
        ye = yp + yd

        if bounding_box.contains(pygame.Rect(xe, ye, 0, 0)):
            """ Moved point is still on screen, so just return the moved
            point with the same direction"""
            # TODO If end point (xe, ye) is on border, the direction
            # should be reversed already
            lines.append(((xp, yp), (xe, ye)))
            return (((xe, ye), (xd, yd)), lines)

        line = bounding_box.clipline(xp, yp, xe, ye)

        lines.append(line)

        rest_line = (line[1], (xe, ye))

        (xp, yp), (xc, yc) = rest_line

        if xp == 0 and xd < 0:
            # reflect across x == 0 line
            xd = - xd
        elif xp == map_width - 1 and xd > 0:
            # reflect across x == map_width - 1 line
            xd = - xd
        if yp == 0 and yd < 0:
            # reflect across y == 0 line
            yd = - yd
        elif yp == map_height - 1 and yd > 0:
            # reflect across y == map_height - 1 line
            yd = - yd

        moving_line = ((xp, yp), (xd, yd))


def main():
    # pygame setup
    pygame.init()
    screen = pygame.display.set_mode((0, 0), pygame.RESIZABLE)

    clock = pygame.time.Clock()
    dt = 0
    running = True

    moving_points = []
    drawn_lines = []

    line_start = None

    screen_width = screen.get_width()
    screen_height = screen.get_height()

    colors = ["blue", "red", "green", "purple"]
    color_idx = -1

    screen.fill("white")
    pygame.display.flip()
    screen.fill("white")
    while running:

        new_moving_points = []

        for ((xp, yp), (xd, yd), color) in moving_points:
            next, lines = reflect_line(
                ((xp, yp), (xd, yd)),
                screen_width,
                screen_height
            )

            ((xp, yp), (xd, yd)) = next

            new_moving_points.append(((xp, yp), (xd, yd), color))

            for a, b in lines:
                pygame.draw.line(screen, color, a, b, 1)

        moving_points = new_moving_points

        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                running = False
            elif event.type == pygame.MOUSEBUTTONDOWN:
                line_start = pygame.mouse.get_pos()
            elif event.type == pygame.MOUSEBUTTONUP:
                pos = pygame.mouse.get_pos()
                color_idx += 1
                if color_idx >= len(colors):
                    color_idx = 0
                dx = (pos[0] - line_start[0]) * dt
                dy = (pos[1] - line_start[1]) * dt
                moving_points.append((line_start, (dx, dy), colors[color_idx]))
                line_start = None

        for (from_pos, to_pos) in drawn_lines:
            pygame.draw.line(screen, "blue", from_pos, to_pos, width=10)

        # flip() the display to put your work on screen
        pygame.display.flip()

        # limits FPS to 60
        # dt is delta time in seconds since last frame, used for framerate-
        # independent physics.
        dt = clock.tick(60) / 1000

    pygame.quit()


if __name__ == '__main__':
    main()
