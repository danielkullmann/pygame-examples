# PyGame Examples

This is a collection of small examples using the PyGame (http://pygame.org) framework.

It currently consists of the following scripts:

- cps.py: A small script that measures your CPS (clicks per second) in 5-second windows
- dots-game.py: A small game where you control a dot that should avoid enemy dots while collecting friendly dots
- dots.py: Dots moving across the screen
- lines0.py: A more elaborate version of lines.py
- lines.py: A script where you can add lines to the screen by clicking with the mouse
- main.py: A very simple script where you move a dot scross the screen
