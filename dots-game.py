"""
Control the large black dot, avoiding the enemies (red and purple)
and try to catch the friends (green). A friend gives lives, an enemy
steals lives (normal enemy (red) one, super enemy (purple) ten).

The number of lives a friend gives depends on the number of enemies
that are on screen, mininum 2, otherwise a 50th of the number of
enemies.

Controls are the arrow keys; Q and Alt-F4 quit game, N restarts game
after losing.
"""

import math
import random
from typing import Dict, List, Set, Tuple

import pygame


# Type defs
Position = List[int]
Point = Tuple[int, int]
PointType = int
MovingPoint = Tuple[Point, Point]
MovingPointWithType = Tuple[MovingPoint, PointType]

ENEMY = 1
FRIEND = 2
SUPER_ENEMY = 3


class Settings:
    # Player settings
    player_size: int = 8
    player_color: str = "black"

    # Enemy / Friend settings
    point_size: int = 5
    creation_frequency: int = 30
    max_points = None
    mininum_friend_fraction: float = 0.1
    friend_factor: int = 25
    create_friends: bool = True
    friends_are_eaten: bool = True
    friends_move: bool = False
    enemies_are_eaten: bool = True
    enemies_hunt: bool = True
    create_super_enemies: bool = True
    enemy_points: int = 1
    super_enemy_points: int = 10
    limited_movement_enemies: bool = True
    initial_bombs: int = 100
    initial_lives: int = 0
    initial_speed: float = 2.0

    # Other settings
    can_lose: bool = True
    initial_points: int = 0
    # (speedup_frequency, speedup_factor)
    speedup: Tuple[int, float] = (300, 1.01)
    # keys: (hit, point_type)
    colors: Dict[Tuple[bool, int], str] = {
        (True, ENEMY): "purple",
        (True, SUPER_ENEMY): "purple",
        (True, FRIEND): "yellow",
        (False, ENEMY): "blue",
        (False, SUPER_ENEMY): "red",
        (False, FRIEND): "green"
    }


high_score = ""


class State:
    def __init__(self, screen_width: int, screen_height: int):
        random.seed(0)
        self.running = True
        self.finished = False
        self.moving_points: List[MovingPointWithType] = []
        self.player: Position = [screen_width//2, screen_height//2]
        self.player_move: Position = [0, 0]
        self.speed = Settings.initial_speed
        self.number_of_lives = Settings.initial_lives
        self.super_enemy_hits: Set[int] = set()
        self.enemy_hits: Set[int] = set()
        self.friend_hits: Set[int] = set()
        self.score = 0
        self.counter = 0
        self.speedup = 1
        self.bombs = Settings.initial_bombs


class Game:

    def distance(self, p1: Point, p2: Point):
        p1x, p1y = p1
        p2x, p2y = p2

        dx = abs(p1x-p2x)
        dy = abs(p1y-p2y)

        return math.sqrt(dx*dx + dy*dy)

    def move_player(self, state: State, screen_width: int, screen_height: int):
        state.player = [
            state.player[0] + state.player_move[0] * state.speed,
            state.player[1] + state.player_move[1] * state.speed
        ]
        if state.player[0] < Settings.player_size:
            state.player[0] = Settings.player_size
        elif state.player[0] >= screen_width-Settings.player_size-1:
            state.player[0] = screen_width-Settings.player_size-1
        if state.player[1] < Settings.player_size:
            state.player[1] = Settings.player_size
        elif state.player[1] >= screen_height-Settings.player_size-1:
            state.player[1] = screen_height-Settings.player_size-1

    def number_of_friends(self, state: State):
        return len([m for m in state.moving_points if m[1] == FRIEND])

    def number_of_enemies(self, state: State):
        return len([m for m in state.moving_points if m[1] != FRIEND])

    def new_point(self, state: State, screen_width: int, screen_height: int):

        if (Settings.max_points is not None and
           Settings.max_points <= len(state.moving_points)):
            return

        while True:
            xp = random.randint(100, screen_width-100)
            yp = random.randint(100, screen_height-100)
            if self.distance((xp, yp), state.player) < 300:
                break

        xd = random.randint(40, 200)
        yd = random.randint(40, 200)

        point_type = ENEMY

        if Settings.create_friends:
            point_type = ENEMY if random.randint(1, 100) < 90 else FRIEND
            if len(state.moving_points) > 0:
                fraction_of_friends = (
                    self.number_of_friends(state) / len(state.moving_points)
                )
                if fraction_of_friends < Settings.mininum_friend_fraction:
                    point_type = FRIEND

            if point_type == FRIEND and not Settings.friends_move:
                xd, yd = 0, 0

        if point_type == ENEMY and Settings.create_super_enemies:
            if random.randint(0, 100) < 30:
                point_type = SUPER_ENEMY

        if point_type in (ENEMY, SUPER_ENEMY):
            if Settings.limited_movement_enemies:
                if random.randint(0, 100) < 50:
                    xd = 0
                else:
                    yd = 0

        state.moving_points.append((((xp, yp), (xd, yd)), point_type))

    def reflect_line(
            self,
            moving_point: MovingPoint,
            map_width: int,
            map_height: int,
            speed: float) -> MovingPoint:
        """ Reflect a moving line across all borders.
        Parameters:
        - moving_point: tuple of (point, vector)
        - point is (int, int)
        - vector is (int, int)
        -.map_width: int
        - map_height: int
        - speed: float

        Returns: tuple of:
        - a new moving_point, i.e. a tuple of (point, vector)
        """

        while True:
            ((xp, yp), (xd, yd)) = moving_point

            xe = int(xp + xd * speed)
            ye = int(yp + yd * speed)

            in_bounds_x = xe >= 0 and xe < map_width
            in_bounds_y = ye >= 0 and ye < map_height

            if in_bounds_x and in_bounds_y:
                # Moved point is still on screen, so just return the moved
                # point with the same direction
                # TODO If end point (xe, ye) is on border, the direction
                # should be reversed already
                return ((xe, ye), (xd, yd))

            bounding_box = pygame.Rect(0, 0, map_width, map_height)
            line = bounding_box.clipline(xp, yp, xe, ye)

            (xp, yp) = line[1]

            if xp == 0 and xd < 0:
                # reflect across x == 0 line
                xd = - xd
            elif xp == map_width - 1 and xd > 0:
                # reflect across x == map_width - 1 line
                xd = - xd
            if yp == 0 and yd < 0:
                # reflect across y == 0 line
                yd = - yd
            elif yp == map_height - 1 and yd > 0:
                # reflect across y == map_height - 1 line
                yd = - yd

            moving_point = ((xp, yp), (xd, yd))

    def kill_neighbors(self, state: State):
        xp, yp = state.player
        new_moving_points = []
        for (((xa, ya), (xd, yd)), point_type) in state.moving_points:
            dx = xp - xa
            dy = yp - ya
            if point_type != FRIEND and dx*dx + dy*dy <= 200*200:
                continue
            new_moving_points.append((((xa, ya), (xd, yd)), point_type))
        state.moving_points = new_moving_points


def main():
    global high_score

    pygame.init()
    pygame.font.init()

    screen = pygame.display.set_mode((0, 0), pygame.RESIZABLE)
    clock = pygame.time.Clock()

    font = pygame.font.SysFont(pygame.font.get_default_font(), 50)

    dt = 1

    screen_width = screen.get_width()
    screen_height = screen.get_height()

    game = Game()
    state = State(screen_width, screen_height)
    for _ in range(Settings.initial_points):
        game.new_point(state, screen_width, screen_height)

    while state.running:

        screen.fill("white")

        new_game = False
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                state.running = False
            elif event.type == pygame.KEYDOWN:
                if event.key == pygame.K_q:
                    state.running = False
                elif event.key == pygame.K_n:
                    if state.finished:
                        new_game = True
                        state = State(screen_width, screen_height)
                        for _ in range(Settings.initial_points):
                            game.new_point(state, screen_width, screen_height)
                elif event.key == pygame.K_b:
                    if state.bombs is not None and state.bombs > 0:
                        state.bombs -= 1
                        game.kill_neighbors(state)
                        pass
                elif event.key == pygame.K_DOWN:
                    state.player_move[1] += 1
                elif event.key == pygame.K_UP:
                    state.player_move[1] -= 1
                elif event.key == pygame.K_LEFT:
                    state.player_move[0] -= 1
                elif event.key == pygame.K_RIGHT:
                    state.player_move[0] += 1
            elif event.type == pygame.KEYUP:
                if event.key == pygame.K_DOWN:
                    state.player_move[1] -= 1
                elif event.key == pygame.K_UP:
                    state.player_move[1] += 1
                elif event.key == pygame.K_LEFT:
                    state.player_move[0] += 1
                elif event.key == pygame.K_RIGHT:
                    state.player_move[0] -= 1

        if state.finished:
            if not new_game:
                continue

        state.counter += 1
        if Settings.creation_frequency > 0 and \
                state.counter % Settings.creation_frequency == 0:
            game.new_point(state, screen_width, screen_height)
            state.score += 1
            if high_score == "" or state.score > high_score:
                high_score = state.score

        if Settings.speedup is not None:
            speedup_frequency, speedup_value = Settings.speedup
            if state.counter % speedup_frequency == 0:
                state.speedup *= speedup_value

        game.move_player(state, screen_width, screen_height)

        pygame.draw.circle(
            screen,
            Settings.player_color,
            state.player,
            Settings.player_size
        )

        new_moving_points = []

        new_super_enemy_hits = set()
        new_enemy_hits = set()
        new_friend_hits = set()

        for index, (moving_point, point_type) \
                in enumerate(state.moving_points):

            next = game.reflect_line(
                moving_point,
                screen_width,
                screen_height,
                dt * state.speedup
            )

            if Settings.enemies_hunt and point_type != FRIEND:
                (ex, ey), (dx, dy) = next
                (px, py) = state.player
                # Measure angle to player
                # Adjust angle towards player
                # Keep speed (length of vector) the same
                next = (ex, ey), (dx, dy)

            hit = (
                game.distance(next[0], state.player) <
                Settings.player_size + Settings.point_size
            )

            color = Settings.colors.get((hit, point_type))

            pygame.draw.circle(screen, color, next[0], Settings.point_size)

            if hit:
                if point_type == ENEMY:
                    new_enemy_hits.add(index)
                elif point_type == SUPER_ENEMY:
                    new_super_enemy_hits.add(index)
                else:
                    new_friend_hits.add(index)

            if hit:
                if point_type == ENEMY and not Settings.enemies_are_eaten:
                    new_moving_points.append((next, point_type))
                if point_type == SUPER_ENEMY and \
                        not Settings.enemies_are_eaten:
                    new_moving_points.append((next, point_type))
                if point_type == FRIEND and not Settings.friends_are_eaten:
                    new_moving_points.append((next, point_type))
            else:
                new_moving_points.append((next, point_type))

        diff = new_enemy_hits.difference(state.enemy_hits)
        if len(diff) > 0:
            state.number_of_lives -= len(diff) * Settings.enemy_points

        diff = new_super_enemy_hits.difference(state.super_enemy_hits)
        if len(diff) > 0:
            state.number_of_lives -= len(diff) * Settings.super_enemy_points

        diff = new_friend_hits.difference(state.friend_hits)
        if len(diff) > 0:
            factor = max(
                2,
                game.number_of_enemies(state) // Settings.friend_factor
            )
            state.number_of_lives += factor * len(diff)

        state.enemy_hits = new_enemy_hits
        state.super_enemy_hits = new_enemy_hits
        state.friend_hits = new_friend_hits

        if Settings.can_lose and state.number_of_lives < 0:
            state.finished = True

        state.moving_points = new_moving_points

        surface1 = font.render(
            "Leben: " + str(state.number_of_lives),
            True,
            "black"
        )
        surface2 = font.render(
            "Punkte: " + str(state.score),
            True,
            "black"
        )
        surface3 = font.render(
            "Feinde: " + str(game.number_of_enemies(state)),
            True,
            "black"
        )
        surface4 = font.render(
            "Freunde: " + str(game.number_of_friends(state)),
            True,
            "black"
        )
        surface5 = font.render(
            "High Score: " + str(high_score),
            True,
            "black"
        )
        surface6 = font.render(
            "Bombs: " + str(state.bombs),
            True,
            "black"
        )

        screen.blit(surface1, (0, 0))
        screen.blit(surface2, (200, 0))
        screen.blit(surface3, (500, 0))
        screen.blit(surface4, (750, 0))
        screen.blit(surface5, (1000, 0))
        screen.blit(surface6, (1300, 0))

        # flip() the display to put your work on screen
        pygame.display.flip()

        # limits FPS to 60
        # dt is delta time in seconds since last frame, used for framerate-
        # independent physics.
        dt = clock.tick(60) / 1000

    print("Final score", state.score)
    pygame.quit()


if __name__ == '__main__':
    main()
